#!/usr/bin/env python3
# Requires pip3 install pms7003

import json
import os
import paho.mqtt.publish as publish
import argparse
import serial

WEATHERBIT_DEVICE = os.getenv('WEATHERBIT_DEVICE', '/dev/ttyACM0')
MQTT_BROKER_HOST = os.getenv('MQTT_BROKER_HOST', 'fluent-bit')
TOPIC = os.getenv('TOPIC', '/demo/weatherbit')

if __name__ == '__main__':
    print("Weatherbit plugin reading from "+WEATHERBIT_DEVICE)

    ser = serial.Serial(WEATHERBIT_DEVICE, 115200, timeout=5)
    ser.flushInput()

    while True:
        rec = ser.readline().decode("utf-8").rstrip()
        
        try:
            y = json.loads(rec)
            try:
                publish.single(TOPIC, json.dumps(y), hostname=MQTT_BROKER_HOST)
            except:
                print("MQTT Connection Issue")
        except:
            print("parse error")
          
